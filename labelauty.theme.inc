<?php

/**
 * @file
 * Labelauty theme functions.
 */

/**
 * Themable display of the Labelauty configuration table.
 */
function theme_labelauty_field_ui_field_edit($variables) {
  $form = $variables['form'];
  $rows = array();

  foreach (element_children($form) as $key) {
    $rows[] = array(
      drupal_render($form[$key]['key']),
      drupal_render($form[$key]['unchecked']),
      drupal_render($form[$key]['checked']),
    );
  }

  $output = theme('table',
    array(
      'header' => array(
        t('Key'),
        t('Unchecked'),
        t('Checked'),
      ),
      'rows' => $rows,
      'attributes' => array('id' => 'labelauty-labels'),
    )
  );
  $output .= drupal_render_children($form);
  return $output;
}
