CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Labelauty module integrates the Labelauty jQuery plugin into Drupal which
provides a nice and lightweight jQuery plugin that gives beauty to checkboxes
and radio buttons and allows custom labels for each status of (un)checked
inputs.

For a full description of the module, visit the project page:
  https://drupal.org/project/labelauty

To submit bug reports and feature suggestions, or to track changes:
  https://drupal.org/project/issues/search/labelauty


REQUIREMENTS
------------

 * Labelauty jQuery plugin: view the installation part of this file for more
  information.


INSTALLATION
------------

 * Module: Install the module as usual, see https://drupal.org/node/895232 for
   further information.

 * Plugin: Download the most recent version of the Labelauty jQuery plugin from
   https://github.com/fntneves/jquery-labelauty. Move the content of the source
   directory into the sites/all/libraries/labelauty directory.


CONFIGURATION
-------------

 * Configure user permissions in Administration » People » Permissions:

   - Administer Labelauty

     Users in roles with the "Administer Labelauty" permission will be able to
     change the configuration of the Labelauty module via Administration »
     User interface » Labelauty.

 * Customize the plugin in Administration » User interface » Labelauty.

 * Enable the Labelauty module for a field by going to the field configuration
   via the field UI and checking the "Use Labelauty plugin" checkbox. This
   enables the possibility to set labels for the checked / unchecked state of
   the field.


MAINTAINERS
-----------

Current maintainers:
 * Mitch Portier (Arkener) - https://drupal.org/user/2284182
