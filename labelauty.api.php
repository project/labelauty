<?php

/**
 * @file
 * API documentation for Labelauty module.
 */

/**
 * Implements hook_labelauty_widget_types().
 *
 * @array $types
 *   Alter this array to add or remove supported widget types.
 */
function hook_labelauty_widget_types(&$types) {
}
