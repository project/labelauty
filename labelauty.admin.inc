<?php

/**
 * @file
 * Labelauty administration form.
 */

/**
 * Menu callback; Labelauty administration form.
 */
function labelauty_form_config($form, &$form_state) {
  $form['config'] = array(
    '#type' => 'vertical_tabs',
  );

  $form['config']['form'] = array(
    '#type' => 'fieldset',
    '#title' => t('Forms'),
    '#description' => t('Configure on which forms the Labelauty plugin should be used. This configuration is ignored on Labelauty enabled checkbox and radio fields on entities.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['config']['form']['labelauty_form'] = array(
    '#type' => 'radios',
    '#title' => t('Show Labelauty on specific forms'),
    '#options' => array(
      LABELAUTY_FORM_NOTLISTED => t('All forms except those listed'),
      LABELAUTY_FORM_LISTED => t('Only the listed forms'),
    ),
    '#default_value' => variable_get('labelauty_form', LABELAUTY_FORM_NOTLISTED),
  );

  $form['config']['form']['labelauty_form_id'] = array(
    '#type' => 'textarea',
    '#title' => t('Forms'),
    '#description' => t('Enter one form id per line.'),
    '#title_display' => 'invisible',
    '#default_value' => variable_get('labelauty_form_id', ''),
  );

  $form['config']['form']['labelauty_form_radio'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Labelauty for radio elements'),
    '#default_value' => variable_get('labelauty_form_radio', TRUE),
  );

  $form['config']['form']['labelauty_form_checkbox'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Labelauty for checkbox elements'),
    '#default_value' => variable_get('labelauty_form_checkbox', TRUE),
  );

  $form['config']['label'] = array(
    '#type' => 'fieldset',
    '#title' => t('Labels'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['config']['label']['labelauty_label_hide'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide labels'),
    '#description' => t('Hide the checked / unchecked labels on elements without a label, displaying only an icon.'),
    '#default_value' => variable_get('labelauty_label_hide', FALSE),
  );

  $form['config']['label']['labelauty_label_checked'] = array(
    '#type' => 'textfield',
    '#title' => t('Default checked label'),
    '#description' => t('The default checked label on elements without a label.'),
    '#default_value' => variable_get('labelauty_label_checked', 'Checked'),
    '#states' => array(
      'visible' => array(
        ':input[name="labelauty_label_hide"]' => array('checked' => FALSE),
      ),
    ),
  );

  $form['config']['label']['labelauty_label_unchecked'] = array(
    '#type' => 'textfield',
    '#title' => t('Default unchecked label'),
    '#description' => t('The default unchecked label on elements without a label.'),
    '#default_value' => variable_get('labelauty_label_unchecked', 'Unchecked'),
    '#states' => array(
      'visible' => array(
        ':input[name="labelauty_label_hide"]' => array('checked' => FALSE),
      ),
    ),
  );

  $form['config']['plugin'] = array(
    '#type' => 'fieldset',
    '#title' => t('Plugin'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['config']['plugin']['labelauty_plugin_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Plugin location'),
    '#default_value' => variable_get('labelauty_plugin_path', LABELAUTY_PLUGIN_PATH),
  );

  return system_settings_form($form);
}
