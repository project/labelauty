/**
 * @file
 * Javascript file for the Labelauty module.
 */

(function ($) {
  'use strict';

  Drupal.behaviors.labelauty = {
    attach: function (context) {
      $('.labelauty-widget:not(.labelauty, .element-invisible, :hidden)', context).each(function () {
        $(this).siblings('label').addClass('element-invisible');
        $(this).labelauty({
          class: 'labelauty',
          same_width: true
        });
      });

      $('.labelauty-form .labelauty-element-hide:not(.labelauty, .element-invisible, :hidden)', context).each(function () {
        $(this).siblings('label').addClass('element-invisible');
        $(this).labelauty({
          class: 'labelauty',
          same_width: true,
          label: false
        });
      });

      $('.labelauty-form .labelauty-element:not(.labelauty, .element-invisible, :hidden)', context).each(function () {
        $(this).siblings('label').addClass('element-invisible');
        $(this).labelauty({
          class: 'labelauty',
          same_width: true
        });
      });
    }
  };
}(jQuery));
